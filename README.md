Exercice RPG
=

Exercice à destination d'étudiants B1. 
Les objectifs de cet exercice sont de découvrir les préceptes de la programmation orientée object.

Profitons-en pour continuer à pratiquer GIT.

Forkez ce dépot, puis travaillez sur votre dépot. 
L'objectif étant, pour chaque exercice, de réaliser un commit. 


N'oubliez pas d'indiquer au formateur l'url pour accéder à votre repository.

Installer le projet.
==
Faites en sorte que le projet soit au sein d'un répertoire suivi par Apache ou Nginx. Et accédez à l'url associée au fichier index.php

Pour info: Si vous avez php d'installé sur votre ordinateur, il vous est possible également de lancer un serveur en réalisant la commande : `php -S localhost:3000` depuis ce dossier. Et donc ne pas avoir besoin de passer par apache.

Exercice 1
==
Lisez le code actuellement présent. Vous avez deux fichiers à voir : `index.php` et `hero.php`.
Faites en sorte pour que lors de l'appel de la méthode `talk`, ce soit le nom du héro qui soit affiché plutôt que uniquement "a Hero".

Comme les héros ne partent jamais seuls en quête créez un deuxième Héro accompagnant Melchior.

Faites le parler également.

Exercice 2
==
Ajoutez des propriétés sur votre Héro. Ainsi que des méthodes.
Partons sur le fait que un Héro dispose de points de vie et de points de fatigue. Ajoutez des propriétés privées pour ces deux propriétés et définissez les getters et setters associés.

Exercice 3
==
Nous allons créer un monstre maintenant. 
Faites une nouvelle classe monstre dans un fichier monstre.php. 
Un monstre aura également un certain nombre de points de vie.

Puis, au sein de la page index, créez une instance de monstre. 
Disons que notre monstre se nomme **Loup affamé**. 

Nous devrions donc avoir 3 objets créés dans la page index à la fin de cet exercice. Deux héros et un monstre.

Exercice 4
==
Héros et Monstres, sont des objets qui ont tendance à ne pas beaucoup apprécier d'être dans la même salle. 
Ils vont donc ici s'attaquer. 

Pour celà, il va d'abord falloir définir un nombre de dégats qu'un héro inflige lors d'une attaque. 
Mais aussi, il va falloir avoir la possibilité d'attaquer un monstre. 

Ajoutez au sein de la classe Héros une nouvelle propriété `degats` ainsi qu'un méthode `attaque`, qui va permettre d'attaquer un monstre donné en paramètres. L'attaque va appeler une méthode publique du monstre nommée `recevoirDegats`. Cette méthode aura en paramètres de fonction, la valeur des dégats infligés.

A chaque fois que le héros va attaquer un monstre, il se fatiguera.

Exercice 5
==
Un **loup affamé** est souvent agressif, d'autant plus si nous lui carressons les côtes avec le fil de notre lame . Implémentez une méthode d'attaque. Ce dernier réalisera une riposte automatique, dès qu'il recoit des dégats. 
A vous les studios pour l'implémentation !